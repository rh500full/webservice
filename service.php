<?php

include("conexion.php");
include("utilidades.php");
include_once 'lib/nusoap.php';


$servicio = new soap_server();
$nameSpace = "urn:examenwsdl";
$servicio->configureWSDL("Examen",$nameSpace);
$servicio->schemaTargetNamespace = $nameSpace;

//registro funcion addUser
$servicio->register("addUser", array('username' => 'xsd:string', 'password' => 'xsd:string', 'email' => 'xsd:string'), array('return' => 'xsd:string'), $nameSpace );

function addUser($username, $password,$email){
   $params = array(
      'username' => $username,
      'password' => $password,
      'email'    => $email
   );
   _log($params,'addUser','access');
   
   $conn = conectar();
   $query = "INSERT INTO usuarios (username,password,email) values('$username','$password','$email')";
   $result = $conn->query($query);

   if($result === TRUE){
      _log(NULL,'addUser','debug');
      return json_encode(array("status_code"=>0));
   }
   else{
      _log($conn->error,'addUser','error');
   }
}


//registro activateUser
$servicio->register("activateUser",array('username' => 'xsd:string'),array('return' => 'xsd:string'),$nameSpace);

function activateUser($username)
{
   $params = array(
      'username' => $username
   );
   _log($params,'activateUser','access');

   $conn = conectar();
   $query = "UPDATE usuarios set active = 1 WHERE username = '$username'";
   $result = $conn->query($query);

   if($result === TRUE){
      _log(NULL,'activateUser','debug');
      return json_encode(array("status_code"=>0));
   }
   else{
      _log($conn->error,'activateUser','error');
   }
}

//registro deactivateUser
$servicio->register("deactivateUser",array('username' => 'xsd:string'),array('return' => 'xsd:string'),$nameSpace);

function deactivateUser($username)
{
   $params = array(
      'username' => $username
   );
   _log($params,'deactivateUser','access');

   $conn = conectar();
   $query = "UPDATE usuarios set active = 0 WHERE username = '$username'";
   $result = $conn->query($query);

   if($result === TRUE){
      _log(NULL,'deactivateUser','debug');
      return json_encode(array("status_code"=>0));
   }
   else{
      _log($conn->error,'deactivateUser','error');
   }
}

//registro de getUser
$servicio->register("getUser",array('username' => 'xsd:string'),array('return' => 'xsd:string'),$nameSpace);

function getUser($username)
{
   $params = array(
      'username'  => $username
   );
   _log($params,'getUser','access');

   $conn = conectar();
   $query = "SELECT username,password,email FROM usuarios where username = '$username'";
   $result = $conn->query($query);
   
   if($result->num_rows > 0){
      _log(NULL,'getUser','debug');
      while($row = $result->fetch_assoc()) {
         $res = array(
            'username' => $row['username'],
            'password' => $row['password'],
            'email'    => $row['email']
         );
      }
      return json_encode($res);
   }
   else{
      _log('No se encontro usuario','getUser','error');
   }
}

$HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';
$servicio->service($HTTP_RAW_POST_DATA);


?>