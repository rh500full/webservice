<?php 
   function _log($params=NULL,$request,$type)
   {
      $res = "";
      $date = "[".date("Y-m-d H:i:s")."] ";
   
      switch ($type) {
         case 'access':
            $res = $date."INFO - Procesando request '$request' ";
            foreach ($params as $key => $value) {
               $res .= "| $key: {$value} ";
            }
            break;
         
         case 'debug':
            $res = $date."INFO - La request '$request' se procesó correctamente";
            break;
         
         case 'error':
            $res = $date."ERROR - Hubo un error al intentar procesar request '$request' error: $params";
      }
      $res .= " \r\n";

      file_put_contents("LOG.txt",$res,FILE_APPEND);
   }
?>